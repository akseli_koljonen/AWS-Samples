import boto3
from botocore.vendored import requests
import json
import datetime
import time

def lambda_handler(event, context):
    for key in event.get('Records'):
        filename = key['s3']['object']['key']
        filesize = key['s3']['object']['size']
    temp = filename.split('/')
    filepath = temp[0] + '/' + temp[1]
    filepath = filepath.replace("%3A", ":")
    s3 = boto3.resource('s3')
    my_bucket = YOURBUCKETNAME
    images = []
    bucket = s3.Bucket(my_bucket)
    for object_summary in bucket.objects.filter(Prefix=filepath):
        images.append(object_summary.key)
    input = {
        "image_name1": images[0],
        "image_name2": images[1],
        "image_name3": images[2]
    }
    output = json.dumps(input)
    # Get the current date and time and change it to correct form for the step function
    date = str(datetime.datetime.now())
    date=date.replace(" ", "-")
    date = date.replace(":", "-")
    # Excecute step function
    client = boto3.client('stepfunctions')
    response = client.start_execution(
    stateMachineArn= YOURSTATEFUNCTIONARN,
    name=date,
    input = output
    )

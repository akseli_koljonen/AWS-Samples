AWS bank application.

--------------------------------------------------------------------------------

These functions are just parts of a bigger application where user signs up
using AWS Cognito. In the sign up process user gives his email as username,
password, full name (FirstnameLastname) and images of front and back
of citizen card and a selfie. Images are stored in S3 bucket. The application
will sign up the user if the verification is successful and sends a new bank card
to the user.  

get_filepath.py  
Gets the filepath of uploaded images from the user (3 images) in the S3 bucket
and executes the step function.  

compare_faces_and_get_info.py  
Gets the filepath of the user's images and compares the two faces from the
citizen card and selfie.
This function uses AWS rekognition to decide if there is a match
between these two images. If there is a match, this function then reads
information from back of the citizen card. The structure of back of the citizen
card must be as follows:  
{  
"FIRSTNAME": "string",  
"LASTNAME": "string",  
"BIRTHDAY": "string",  
"SOCIALNUMBER": "string",  
"CIVILID": "string",  
"TAXNUMBER": "string",  
"HEALTHNUMBER": "string"  
}  
If there is not a match, this function returns 0 and passes the decision to an
activity worker.  

get_activity.py  
Gets the activity from the previous function. This function will download the
images of the user for the worker who will decide whether there is a match or
not.

import boto3
from botocore.vendored import requests
import json


def lambda_handler(event, context):
    print(event)
    client = boto3.client('rekognition')
    s3 = boto3.resource('s3')
    my_bucket = YOURBUCKETNAME # Insert the name of your bucket here.
    SIMILARITY_THRESHOLD = 50.0
    bucket_found = False

    # Check if bucket is found
    for bucket in s3.buckets.all():
        if bucket.name == my_bucket:
            print('Bucket found')
            bucket_found = True

    if not bucket_found:
        print('S3 bucket not found. Check your bucket name and permissions from IAM')
    bucket = s3.Bucket(my_bucket)
    front_citizen_image = event['image_name1']
    back_citizen_image = event['image_name2']
    selfie_image = event['image_name3']
    comparision_result = face_comparision(client, my_bucket, front_citizen_image, selfie_image, SIMILARITY_THRESHOLD)
    if comparision_result == 0: # No match
        value = {
                    'value' : 0,  # send 0 for failure
                    'image_name1' : selfie_image,
                    'image_name2' : front_citizen_image
            }
        return value
    user_information = ocr(client, my_bucket, back_citizen_image) # information of the user from the card
    value = '{"value":1}' # send 1 for succees
    dictB = json.loads(value)
    dictA = user_information
    # Merge variables 'value' and 'user_information' to one JSON output and return it
    result = dictB.copy(); result.update(dictA)
    return result


def face_comparision(client, bucket, front_citizen, selfie, SIMILARITY_THRESHOLD):
    my_bucket = bucket
    front_citizen_image = front_citizen
    selfie_image = selfie
    rekognition_response = client.compare_faces(
                   SourceImage={
                        'S3Object':{'Bucket':my_bucket,'Name':front_citizen_image}},
                   TargetImage={
                        'S3Object':{'Bucket':my_bucket,'Name':selfie_image}},
                   SimilarityThreshold=SIMILARITY_THRESHOLD
    )
    for faceMatch in rekognition_response['FaceMatches']:
        position = faceMatch['Face']['BoundingBox']
        confidence = str(faceMatch['Face']['Confidence'])
        print('The face at ' +
                       str(position['Left']) + ' ' +
                       str(position['Top']) +
                       ' matches with ' + confidence + '% confidence')
    if len(rekognition_response['FaceMatches']) != 0:
        print("We have a match with a confidence of: {0:.3f}".format(float(confidence)))
        print("Processing to OCR... \n")
    else:
        print("No match")
        print("Exiting...")
        return 0



def ocr(client, bucket, name):
    my_bucket = bucket
    back_citizen_image = name
    text_response = client.detect_text(
            Image = {
                'S3Object':{'Bucket':my_bucket,'Name':back_citizen_image}}
        )
    card_information = {}
    i = 0
    error_msg = "Not found"
    for words in text_response['TextDetections']:
            word = words['DetectedText']
            type = words['Type']
            if str(type) == 'LINE': # Only take lines from the citizen card
                tmp = word.split(':')
                try:
                    card_information[tmp[0]] = tmp[1]
                except IndexError:
                    card_information[tmp[0]] = error_msg
                    print("Error in reading the card. Infromation could not be read correctly. Error %1d" % i)
                i += 1
    # Create JSON output
    firstname = str(card_information["FIRSTNAME"])
    lastname = str(card_information["LASTNAME"])
    fullname = firstname + lastname
    email, email_status = get_email(fullname)
    if email_status == False:
        print("Email of user {} {} could not be found".format(firstname, lastname))
    card_information['EMAIL'] = email
    card_information_json = json.dumps(card_information)
    card_information_json = json.loads(card_information_json)
    return card_information_json # Information from the card in JSON


def get_email(FULLNAME):
    client = boto3.client('cognito-idp')
    response = client.list_users(
    UserPoolId='eu-west-1_o2yjSGcoL',
    Limit=10,
    )
    amount = len(response["Users"])
    status = False
    for x in range(0,amount):
        if response["Users"][x]["Attributes"][2]["Value"].lower() == FULLNAME.lower():
            status = True
            email = response["Users"][x]["Attributes"][3]["Value"]
            break;
        email = None
    return email, status

import boto3
import botocore
import json

client = boto3.client('stepfunctions')
my_bucket = YOURBUCKETNAME
s3 = boto3.resource('s3')
while True:

    list_response = client.list_activities(
    maxResults=123)
    for activites in list_response['activities']:
        activity = activites['activityArn']


    activity_response = client.get_activity_task(
        activityArn = activity
    )
    activity_token = activity_response['taskToken']
    input_data = json.loads(activity_response['input'])
    # Failed images (names)

    image1 = input_data['image_name1']
    image2 = input_data['image_name2']
    KEY1 = image1
    KEY2 = image2
    try:
        s3.Bucket(my_bucket).download_file(KEY1, image1)
        s3.Bucket(my_bucket).download_file(KEY2, image2)
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The images does not exist.")
        else:
            raise
    # Worker must decide if images match or not
    decision = input("Is there a match? Give 1 for match and 0 for no match:\n ")
    while decision != '1' and decision != '0':
        print("Wrong input!")
        print("Give 1 for match and 0 for no match:")
        decision = input('Is there a match?\n')

    if decision == "1":
        value = {"foo" : 1}
    else:
        value = {"foo" : 0}
    output = json.dumps(value)
    print("No match!")
    response = client.send_task_success(
        taskToken = activity_token,
        output = output
    )
